package edu.umuc.validation.aspects;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.executable.ExecutableValidator;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.ConstructorSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;

import edu.umuc.logging.annotations.NoAutoLogging;

/**
 * Automatically apply JSR-303 and JSR-349 validation to annotated fields and methods.
 * 
 * @author Christopher Febles
 *
 */
@Aspect
@NoAutoLogging
public final class ValidationAspect {

    @Autowired
    private ExecutableValidator validator;

    private ValidationAspect() {
    }

    // Validate Methods

    @Pointcut( "execution(* edu.umuc.validation.aspects..*(..))" )
    public void validationAspectMethods() {
    }

    @Pointcut( "execution(* edu.umuc.validation.configuration..*(..))" )
    public void validationConfigurationMethods() {
    }

    @Before( "execution(* *(..)) && !validationAspectMethods() && !validationConfigurationMethods()" )
    public void validateMethodParameters( final JoinPoint joinPoint ) {

        final Method method = this.getMethod( joinPoint );
        if ( method == null ) {
            return;
        }
        
        Object target = joinPoint.getTarget();
        if ( target == null ) {
            // JSR-349 does not support validation of static methods
            return;
        }

        final Set<ConstraintViolation<Object>> violations = validator.validateParameters( target, method, joinPoint.getArgs() );
        if ( !violations.isEmpty() ) {
            // Validation failed
            throw new ConstraintViolationException( violations );
        }
    }

    @AfterReturning( pointcut = "execution(* *(..)) && !validationAspectMethods() && !validationConfigurationMethods()", returning = "retVal" )
    public void validateMethodReturnValue( final JoinPoint joinPoint, final Object retVal ) {

        final Method method = this.getMethod( joinPoint );
        if ( method == null ) {
            return;
        }
        
        Object target = joinPoint.getTarget();
        if ( target == null ) {
            // JSR-349 does not support validation of static methods
            return;
        }

        final Set<ConstraintViolation<Object>> violations = validator.validateReturnValue( target, method, retVal );
        if ( !violations.isEmpty() ) {
            // Validation failed
            throw new ConstraintViolationException( violations );
        }
    }

    // Validate Constructors

    @Pointcut( "execution(edu.umuc.validation.aspects..*.new(..))" )
    public void validationAspectConstructors() {
    }

    @Pointcut( "execution(edu.umuc.validation.configuration..*.new(..))" )
    public void validationConfigurationConstructors() {
    }

    @Before( "execution(edu.umuc..*.new(..)) && !validationAspectConstructors() && !validationConfigurationConstructors()" )
    public void validateConstructor( final JoinPoint joinPoint ) {

        final Constructor<?> constructor = this.getConstructor( joinPoint );
        if ( constructor == null ) {
            return;
        }

        // Validate parameters
        final Object[] args = joinPoint.getArgs();
        if ( args.length > 0 ) {
            final Set<ConstraintViolation<Object>> violations = validator.validateConstructorParameters( constructor, args );

            if ( !violations.isEmpty() ) {
                // Validation failed
                throw new ConstraintViolationException( violations );
            }
        }
    }

    @AfterReturning( pointcut = "call(edu.umuc..*.new(..)) && !validationAspectConstructors() && !validationConfigurationConstructors()", returning = "retVal" )
    public void validateConstructorReturnValue( final JoinPoint joinPoint, final Object retVal ) {

        final Constructor<?> constructor = this.getConstructor( joinPoint );
        if ( constructor == null ) {
            return;
        }

        final Set<ConstraintViolation<Object>> violations = validator.validateConstructorReturnValue( constructor, retVal );
        if ( !violations.isEmpty() ) {
            // Validation failed
            throw new ConstraintViolationException( violations );
        }
    }

    /**
     * Get the java.lang.reflect.Constructor object from the given JoinPoint.
     * 
     * @param joinPoint
     *            The JoinPoint provided by AspectJ. Cannot be null.
     * @return The Constructor, if available. Null otherwise.
     */
    private Constructor<?> getConstructor( final JoinPoint joinPoint ) {

        // When using PowerMock, sometimes the StaticPart is null.
        if ( joinPoint.getStaticPart() == null
                || !( joinPoint.getKind().equals( JoinPoint.CONSTRUCTOR_EXECUTION ) || joinPoint.getKind().equals( JoinPoint.CONSTRUCTOR_CALL ) ) ) {
            return null;
        }

        final ConstructorSignature signature = ( ConstructorSignature ) joinPoint.getSignature();
        return signature.getConstructor();
    }

    /**
     * Get the java.lang.reflect.Method object from the given JoinPoint.
     * 
     * @param joinPoint
     *            The JoinPoint provided by AspectJ. Cannot be null.
     * @return The Method, if available. Null otherwise.
     */
    private Method getMethod( final JoinPoint joinPoint ) {

        // When using PowerMock, sometimes the StaticPart is null.
        if ( joinPoint.getStaticPart() == null || !joinPoint.getKind().equals( JoinPoint.METHOD_EXECUTION ) ) {
            return null;
        }

        final MethodSignature signature = ( MethodSignature ) joinPoint.getSignature();
        return signature.getMethod();
    }
}

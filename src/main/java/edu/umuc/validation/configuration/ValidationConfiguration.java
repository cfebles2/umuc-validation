package edu.umuc.validation.configuration;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;

import org.aspectj.lang.Aspects;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.parameternameprovider.ReflectionParameterNameProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import edu.umuc.validation.aspects.ValidationAspect;

@Configuration
@ComponentScan( { "edu.umuc.validation" } )
@EnableAspectJAutoProxy
public class ValidationConfiguration {
    
    @Bean
    public ExecutableValidator validator() {
        final ValidatorFactory factory = Validation.byProvider( HibernateValidator.class ).configure() //
                .parameterNameProvider( new ReflectionParameterNameProvider() ) //
                .buildValidatorFactory();
        return factory.getValidator().forExecutables();
    }
    
    @Bean
    public ValidationAspect aspect() {
        return Aspects.aspectOf( ValidationAspect.class );
    }

}

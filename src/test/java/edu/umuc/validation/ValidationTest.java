package edu.umuc.validation;

import static org.junit.Assert.*;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.umuc.validation.configuration.ValidationConfiguration;
import edu.umuc.validation.test.ValidationTestClass;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = { ValidationConfiguration.class } )
public class ValidationTest {

    private ValidationTestClass obj;
    
    @Before
    public void setUp() {
        obj = new ValidationTestClass();
    }
    
    @Test( expected = ConstraintViolationException.class )
    public void testNotNullableParameter() {
        obj.testMethod( null );
    }

    @Test
    public void testValidParameter() {
        assertEquals( 1, obj.testMethod( "a" ) );
    }

    @Test( expected = ConstraintViolationException.class )
    public void testTooShortReturnValue() {
        obj.testMethod( "" );
    }
    
    @Test
    public void testNonValidatedMethod() {
        assertEquals( "String", obj.nonValidatedMethod( "String" ) );
    }
    
    @Test
    public void testNonValidatedMethodWithNoArgs() {
        obj.nonValidatedMethod();
    }

    @Test( expected = ConstraintViolationException.class )
    public void testValidConstructor() {
        new ValidationTestClass( null );
    }

    @Test( expected = ConstraintViolationException.class )
    public void testNonNullConstructorParameters() {
        new ValidationTestClass( null, null );
    }
    
    @Test( expected = ConstraintViolationException.class )
    public void testStaticMethodReturnValidator() {
        ValidationTestClass.newInstance( null );
    }
    
    @Test( expected = ConstraintViolationException.class )
    public void testStaticMethodParameterValidator() {
        ValidationTestClass.newInstance( null, null );
    }
}

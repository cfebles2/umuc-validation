package edu.umuc.validation.test;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.powermock.reflect.Whitebox;

public class ValidationTestClass {
    
    @NotNull
    private final String value;
    
    public ValidationTestClass() {
        value = "";
    }
    
    @Valid
    public ValidationTestClass( final String value ) {
        this.value = value;
    }
    
    public ValidationTestClass( @NotNull final String value, @NotNull final String value2 ) {
        this.value = value + value2;
    }
    
    @Min( 1 )
    public int testMethod( @NotNull final String aStringParameter ) {
        return aStringParameter.length();
    }
    
    public String nonValidatedMethod( final String aStringParameter ) {
        return aStringParameter;
    }
    
    public void nonValidatedMethod() {
    }
    
    @Valid
    public static ValidationTestClass newInstance( final String value ) {
        final ValidationTestClass val = new ValidationTestClass();
        
        Whitebox.setInternalState( val, String.class, value );
        
        return val;
    }
    
    public static ValidationTestClass newInstance( @NotNull final String value, @NotNull final String value2 ) {
        final ValidationTestClass val = new ValidationTestClass();
        
        Whitebox.setInternalState( val, String.class, value + value2 );
        
        return val;
    }

}
